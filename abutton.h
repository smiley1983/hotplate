/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef ABUTTONH
#define ABUTTONH

#ifdef CRYSTAL
  #define DEBOUNCE_TICKS 600
  #define DEBOUNCE_SETTLE 48
#else
  #define DEBOUNCE_TICKS 400
  #define DEBOUNCE_SETTLE 32
#endif

#include <avr/io.h>
#include <avr/interrupt.h>

struct abutton {
  uint16_t reading;
  uint8_t pressed;
  uint8_t value_ready;
  uint8_t value;
  uint16_t debounce;
  uint8_t repeat;
};

void init_abutton(void);

void update_abutton_reading(void);
uint16_t get_abutton_reading(void);

#endif

