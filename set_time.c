/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <set_time.h>
#include <clock.h>
#include <button.h>


#define SET_HOURS 0
#define SET_MINUTES 1

#ifdef CRYSTAL
  #define FLASH_ON_TICKS 600
  #define FLASH_OFF_TICKS 300
#else
  #define FLASH_ON_TICKS 75
  #define FLASH_OFF_TICKS 38
#endif

uint8_t sub_mode = SET_HOURS;

uint16_t flash_delay = 0;
uint8_t flash_state = 0;

uint8_t s_minutes = 0;
uint8_t s_hours = 0;

void update_flash(void) {
  if (flash_delay > 0) {
    flash_delay--;
  }
  else {
    flash_state = !flash_state;
    if (flash_state) {
      flash_delay = FLASH_ON_TICKS;
    }
    else {
      flash_delay = FLASH_OFF_TICKS;
    }
  }
}

void set_time_menu(uint8_t *shift_data) {
  if (sub_mode == SET_HOURS) {
    update_display_string(s_hours, s_minutes);
    update_flash();
    clock_display_flash_hours(flash_state, shift_data);
  }
  else if (sub_mode == SET_MINUTES) {
    update_display_string(s_hours, s_minutes);
    update_flash();
    clock_display_flash_minutes(flash_state, shift_data);
  }
}

void increment_w60(uint8_t *i) {
  if ((*i) < 59) (*i)++;
  else (*i) = 0;
}

void decrement_w60(uint8_t *i) {
  if ((*i) == 0) (*i) = 59;
  else (*i)--;
}

void increment_w24(uint8_t *i) {
  if ((*i) < 23) (*i)++;
  else (*i) = 0;
}

void decrement_w24(uint8_t *i) {
  if ((*i) == 0) (*i) = 23;
  else (*i)--;
}

void set_time_process_buttons(uint8_t *menu_mode, uint8_t *shift_data, timeset_func set_time) {
  set_time_menu(shift_data);
  if (up_pressed()) {
    clear_button_states();
    update_display_string(clock_hours, clock_minutes);
    (*menu_mode) = 0;
  }
  if (sub_mode == SET_HOURS) {
    if (down_pressed()) {
      clear_button_states();
      sub_mode = SET_MINUTES;
    }
    else if (left_pressed()) {
      clear_button_states();
      decrement_w24(&s_hours);
    }
    else if (right_pressed()) {
      clear_button_states();
      increment_w24(&s_hours);
    }
  }
  if (sub_mode == SET_MINUTES) {
    if (down_pressed()) {
      clear_button_states();
//      set_clock_time(s_hours, s_minutes);
      set_time(s_hours, s_minutes);
      sub_mode = SET_HOURS;
      (*menu_mode) = 0;
    }
    else if (left_pressed()) {
      clear_button_states();
      decrement_w60(&s_minutes);
    }
    else if (right_pressed()) {
      clear_button_states();
      increment_w60(&s_minutes);
    }
  }
}

void begin_setting_time(uint8_t h, uint8_t m) {
  s_minutes = m;
  s_hours = h;
  sub_mode = SET_HOURS;
}

