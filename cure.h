/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef CUREH
#define CUREH

#include <avr/io.h>
#include <macro.h>
#include <pin.h>
#include <abutton.h>
#include <led.h>

#define TEST_SETTINGS 24

/* test
 */
#define TEST1 100
#define TEST2 150
#define TEST3 200
#define TEST4 250
#define TEST5 300
#define TEST6 350

#define TEST7 400
#define TEST8 450
#define TEST9 500
#define TEST10 550
#define TEST11 600
#define TEST12 650

#define TEST13 700
#define TEST14 750
#define TEST15 800
#define TEST16 850
#define TEST17 900
#define TEST18 910

#define TEST19 920
#define TEST20 930
#define TEST21 940
#define TEST22 950
#define TEST23 960
#define TEST24 965

#endif

