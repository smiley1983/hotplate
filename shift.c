/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

//#include <stdlib.h>
#include <shift.h>
#include <pin.h>

void data_on (void) { output_high (PORTB, SHIFT_OUT); }
void data_off (void) { output_low (PORTB, SHIFT_OUT); }

void shift_on (void) { output_high (PORTB, SHIFT_CLK); }
void shift_off (void) { output_low (PORTB, SHIFT_CLK); }

void store_on (void) { output_high (PORTB, STORE_CLK); }
void store_off (void) { output_low (PORTB, STORE_CLK); }

#define DIGIT0 15
#define DIGIT1 12
#define DIGIT2 11
#define DIGIT3 9
#define SEGTOP 14
#define SEGTOPLEFT 13
#define SEGTOPRIGHT 10
#define SEGMIDDLE 8
#define SEGBOTTOMRIGHT 7
#define SEGDOT 6
#define SEGBOTTOM 5
#define SEGBOTTOMLEFT 4

#define DIGITS (1 << 15) | (1 << 12) | (1 << 11) | (1 << 9)

struct byte_pair bytes_of_uint16_t (uint16_t v) {
  struct byte_pair b;
  b.bl = v & 0xff;
  b.bh = (v >> 8) & 0xff;
  return b;
}

uint16_t get_char_bits (char c) {
  switch (c) {
    case '?': // Empty
      //       d  dd d
      //     0b1001101000000000;
      return 0b1001101000000000;
    case '0':
    case 'Q':
      return 0b1111111010110000;
    case '1':
      return 0b1001111010000000;
    case '2':
    case 'Z':
      return 0b1101111100110000;
    case '3':
    case 'M':
    case 'W':
      return 0b1101111110100000;
    case 'Y':
    case '4':
      return 0b1011111110000000;
    case '5':
    case 'S':
      return 0b1111101110100000;
    case '6':
    case 'G':
      return 0b1111101110110000;
    case '7':
    case 'T':
      return 0b1101111010000000;
    case '8':
    case 'B':
      return 0b1111111110110000;
    case '9':
      return 0b1111111110100000;
    case 'A':
      return 0b1111111110010000;
    case 'C':
      return 0b1111101000110000;
    case 'D':
      return 0b1001111110110000;
    case 'E':
      return 0b1111101100110000;
    case 'F':
      return 0b1111101100010000;
    case 'X':
    case 'K':
    case 'H':
      return 0b1011111110010000;
    case 'N':
      return 0b1001101110010000;
    case 'U':
    case 'V':
      return 0b1011111010110000;
    case 'O':
      return 0b1001101110110000;
    case 'I':
      return 0b1011101000010000;
    case 'L':
      return 0b1011101000110000;
    case 'J':
      return 0b1001111010100000;
    case 'P':
      return 0b1111111100010000;
    case 'R':
      return 0b1001101100010000;
    case '-':
      return 0b1001101100000000;
    default:
      return 0b1001101000000000;
  }
}

uint16_t get_shift_digit (uint8_t i) {
  switch (i) {
    case 0: return 15;
    case 1: return 12;
    case 2: return 11;
    default : return 9;
  }
}

void shift_output (uint16_t bits) {
  uint16_t i = 0;
  store_off();
  for (i = 0; i < SHIFT_BITS; i++) {
    data_off();
    shift_off();
    store_off();
    if (bits & (1 << i) ) {
      data_on();
    }
    shift_on();
  }
  store_on();
  shift_off();
  store_off();
}

void shift_byte (uint8_t byte) {
  for (uint8_t i = 0; i < BYTE; i++) {
    data_off();
    shift_off();
    store_off();
    if (byte & (1 << i)) {
      data_on();
    }
    shift_on();
  };
}

void shift_bytes (uint8_t *byte) {
  store_off();
  for (uint8_t index = 0; index < SHIFT_BYTES; index++) {
    shift_byte (byte[index]);
  }
  store_on();
  shift_off();
  store_off();
}

void init_shift(void) {
  // initialize the direction of the ports to be outputs
  // on the 3 pins connected to the first shift register
  set_output(DDRB, SHIFT_OUT);  
  set_output(DDRB, SHIFT_CLK);
  set_output(DDRB, STORE_CLK);

  output_high(PORTB, SHIFT_OUT);
  output_low(PORTB, SHIFT_CLK);
  output_high(PORTB, STORE_CLK);
  
}

