/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <led.h>
#include <macro.h>
#include <pin.h>

uint8_t led1_state = 0;
uint8_t led2_state = 0;
uint8_t led3_state = 0;
uint8_t led4_state = 0;
uint8_t led5_state = 0;
uint8_t led6_state = 0;
uint8_t led7_state = 0;
uint8_t led8_state = 0;

void init_leds(void) {
  set_output(LED1_PORTDIR, LED1);
  set_output(LED2_PORTDIR, LED2);
  set_output(LED3_PORTDIR, LED3);
  set_output(LED4_PORTDIR, LED4);
  set_output(LED5_PORTDIR, LED5);
  set_output(LED6_PORTDIR, LED6);

  output_low(LED1_PORT, LED1);
  output_low(LED2_PORT, LED2);
  output_low(LED3_PORT, LED3);
  output_low(LED4_PORT, LED4);
  output_low(LED5_PORT, LED5);
  output_low(LED6_PORT, LED6);
}

void led1_on(void) {
  output_high(LED1_PORT, LED1);
  led1_state = 1;
}

void led2_on(void) {
  output_high(LED2_PORT, LED2);
  led2_state = 1;
}

void led3_on(void) {
  output_high(LED3_PORT, LED3);
  led3_state = 1;
}

void led4_on(void) {
  output_high(LED4_PORT, LED4);
  led4_state = 1;
}

void led1_off(void) {
  output_low(LED1_PORT, LED1);
  led1_state = 0;
}

void led2_off(void) {
  output_low(LED2_PORT, LED2);
  led2_state = 0;
}

void led3_off(void) {
  output_low(LED3_PORT, LED3);
  led3_state = 0;
}

void led4_off(void) {
  output_low(LED4_PORT, LED4);
  led4_state = 0;
}

void led5_on(void) {
  output_high(LED5_PORT, LED5);
  led5_state = 1;
}

void led6_on(void) {
  output_high(LED6_PORT, LED6);
  led6_state = 1;
}

void led5_off(void) {
  output_low(LED5_PORT, LED5);
  led5_state = 0;
}

void led6_off(void) {
  output_low(LED6_PORT, LED6);
  led6_state = 0;
}

void toggle_led(ledset_func set_on, ledset_func set_off, uint8_t *state) {
  (*state) = !(*state);
  if (*state) {
    set_on();
  }
  else {
    set_off();
  };
}

void toggle_led1(void) {
  toggle_led(led1_on, led1_off, &led1_state);
}

void toggle_led2(void) {
  toggle_led(led2_on, led2_off, &led2_state);
}

void toggle_led3(void) {
  toggle_led(led3_on, led3_off, &led3_state);
}

void toggle_led4(void) {
  toggle_led(led4_on, led4_off, &led4_state);
}

void toggle_led5(void) {
  toggle_led(led5_on, led5_off, &led5_state);
}

void toggle_led6(void) {
  toggle_led(led6_on, led6_off, &led6_state);
}

#ifdef BOARD_B

void green1_on(void) {
  led3_on();
}

void green1_off(void) {
  led3_off();
}

void red1_on(void) {
  led4_on();
}

void red1_off(void) {
  led4_off();
}

void blue1_on(void) {
  led2_on();
}

void blue1_off(void) {
  led2_off();
}

void green2_on(void) {
  led6_on();
}

void green2_off(void) {
  led6_off();
}

void red2_on(void) {
  led1_on();
}

void red2_off(void) {
  led1_off();
}

void blue2_on(void) {
  led5_on();
}

void blue2_off(void) {
  led5_off();
}

#else

void green1_on(void) {
  led6_on();
}

void green1_off(void) {
  led6_off();
}

void red1_on(void) {
  led5_on();
}

void red1_off(void) {
  led5_off();
}

void blue1_on(void) {
  led4_on();
}

void blue1_off(void) {
  led4_off();
}

void green2_on(void) {
  led2_on();
}

void green2_off(void) {
  led2_off();
}

void red2_on(void) {
  led3_on();
}

void red2_off(void) {
  led3_off();
}

void blue2_on(void) {
  led1_on();
}

void blue2_off(void) {
  led1_off();
}

#endif
