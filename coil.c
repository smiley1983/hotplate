/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <coil.h>

uint16_t coil_seconds = 0;

uint8_t heat_setting = 0;

uint8_t coil_mode = 0;

uint8_t manual_on = 0;

uint16_t heat_value[HEAT_SETTINGS];

//uint16_t test_value[TEST_SETTINGS];

void next_heat(void) {
  heat_setting++;
  if (heat_setting >= HEAT_SETTINGS) {
    heat_setting = 0;
  }
}

void prev_heat(void) {
  if (heat_setting == 0) {
    heat_setting = HEAT_SETTINGS;
  }
  heat_setting--;
}

void init_coil(void) {

  set_output(COIL_PORTDIR, COIL);
  output_low(COIL_PORT, COIL);

  heat_value[0] = SAFE;
  heat_value[1] = HOT1;
  heat_value[2] = HOT2;
  heat_value[3] = HOT3;
  heat_value[4] = HOT4;
  heat_value[5] = HOT5;

/*
  test_value[0] = TEST1;
  test_value[1] = TEST2;
  test_value[2] = TEST3;
  test_value[3] = TEST4;
  test_value[4] = TEST5;
  test_value[5] = TEST6;
  test_value[6] = TEST7;
  test_value[7] = TEST8;
  test_value[8] = TEST9;
  test_value[9] = TEST10;
  test_value[10] = TEST11;
  test_value[11] = TEST12;
  test_value[12] = TEST13;
  test_value[13] = TEST14;
  test_value[14] = TEST15;
  test_value[15] = TEST16;
  test_value[16] = TEST17;
  test_value[17] = TEST18;
  test_value[18] = TEST19;
  test_value[19] = TEST20;
  test_value[20] = TEST21;
  test_value[21] = TEST22;
  test_value[22] = TEST23;
  test_value[23] = TEST24;
*/

}

void coil_on(void) {
  output_high(COIL_PORT, COIL);
}

void coil_off(void) {
  output_low(COIL_PORT, COIL);
}

void coilmode_on(void) {
  coil_mode = 1;
}

void coilmode_off(void) {
  coil_mode = 0;
}

void toggle_coilmode(void) {
  coil_mode = !coil_mode;
  if (!coil_mode) {
    coil_seconds = 0;
  }
}

void update_coil_leds(void) {
  update_abutton_reading();
  uint16_t abutton_reading = get_abutton_reading();
  if (abutton_reading < SAFE) {
    //led2_on();
    //led1_off();
    //led3_off();
    green2_on();
    blue2_off();
    red2_off();
  }
  else if (abutton_reading < HOT1) {
    //led3_on();
    //led1_off();
    //led2_off();
    green2_off();
    blue2_off();
    red2_on();
  }
  else if (abutton_reading < heat_value[heat_setting]) {
    //led1_on();
    //led3_off();
    //led2_off();
    green2_off();
    blue2_on();
    red2_off();
  }
  else {
    //led1_on();
    //led3_on();
    //led2_on();
    green2_on();
    blue2_on();
    red2_on();
  }
}

uint8_t coil_state(void) {
  return (coil_mode != 0);
}

void update_coil_setting_leds(void) {
  if (coil_state()) {
      blue1_on();
      green1_off();
      red1_off();
      //led4_on();
      //led5_off();
      //led6_off();
  }
  else {
    if (heat_setting == 0) {
      blue1_off(); // blue
      red1_off(); // red
      green1_off(); // green
    }
    else if (heat_setting == 1) {
      blue1_off(); // blue
      red1_off(); // red
      green1_on(); // green
    }
    else if (heat_setting == 2) {
      blue1_off(); // blue
      red1_on(); // red
      green1_off(); // green
    }
    else if (heat_setting == 3) {
      blue1_on(); // blue
      red1_on(); // red
      green1_off(); // green
    }
    else if (heat_setting == 4) {
      blue1_on(); // blue
      red1_off(); // red
      green1_on(); // green
    }
    else {
      blue1_on();
      red1_on();
      green1_on();
    }
  }
}

void update_coil_seconds(void) {
  if (coil_seconds > 0) {
    coil_seconds--;
  };
  update_coil();
}

void update_coil(void) {
//  if (coil_seconds > 0) {
//    coil_seconds--;
//  };
  if (coil_seconds == 0 && !manual_on) {
    coil_off();
  };
  if (coil_seconds == 0) {
    coil_mode = 0;
  };
  update_coil_leds();
}

void regulate_coil(void) {
  update_abutton_reading();
  uint16_t abutton_reading = get_abutton_reading();
  if (abutton_reading < heat_value[heat_setting]) {
    coil_on();
  }
  else {
    coil_off();
  }
}

void set_coil_cycle(uint16_t seconds) {
  coil_seconds = seconds;
  regulate_coil();
}

void set_manual_on(void) {
  manual_on = 1;
}

void unset_manual_on(void) {
  manual_on = 0;
}


