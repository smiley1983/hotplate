/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


#ifndef COILH
#define COILH

#define HEAT_SETTINGS 6
#define TEST_SETTINGS 24

/* test1
#define SAFE 100
#define HOT1 150
#define HOT2 200
#define HOT3 250
#define HOT4 300
#define HOT5 350
 */

/* test2
#define SAFE 100
#define HOT1 350
#define HOT2 400
#define HOT3 450
#define HOT4 500
#define HOT5 550
 */

/* test3
#define SAFE 100
#define HOT1 600
#define HOT2 650
#define HOT3 700
#define HOT4 750
#define HOT5 800
 */

/* test4
#define SAFE 150
#define HOT1 800
#define HOT2 850
#define HOT3 900
#define HOT4 940
#define HOT5 965
 */

/* good
*/
#define SAFE 150
#define HOT1 850
#define HOT2 920
#define HOT3 925
#define HOT4 930
#define HOT5 935

#include <avr/io.h>
#include <macro.h>
#include <pin.h>
#include <abutton.h>
#include <led.h>

void init_coil(void);
void update_coil(void);
void update_coil_seconds(void);
uint8_t coil_state(void);
void coil_on(void);
void coil_off(void);
void prev_heat(void);
void next_heat(void);
void coilmode_on(void);
void coilmode_off(void);
void toggle_coilmode(void);
void update_coil_setting_leds(void);
void regulate_coil(void);
void set_manual_on(void);
void unset_manual_on(void);
void set_coil_cycle(uint16_t seconds);

#endif

