/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include <clock.h>
#include <shift.h>
#include <led.h>
#include <alarm.h>
#include <coil.h>

uint8_t clock_seconds = 0;
uint8_t clock_minutes = 0;
uint8_t clock_hours = 0;
uint8_t digit_offset = 0;

// set a starting message when the device is first switched on
char display_string[NUM_DISPLAY_CHARS] = "CATS";

void set_clock_time(uint8_t hours, uint8_t minutes) {
  clock_seconds = 0;
  clock_minutes = minutes;
  clock_hours = hours;
  update_display_string(hours, minutes);
}

char digit_of_int(uint8_t v) {
  return v + '0';
}

void display_four_digit_int(uint16_t v) {
  display_string[0] = digit_of_int ((v % 9999) / 1000);
  display_string[1] = digit_of_int (((v % 9999) / 100) % 10);
  display_string[2] = digit_of_int (((v % 9999) / 10) % 10);
  display_string[3] = digit_of_int ((v % 9999) % 10);
}

void update_display_string(uint8_t hours, uint8_t minutes) {
  display_string[0] = digit_of_int(hours / 10);
  display_string[1] = digit_of_int(hours % 10);
  display_string[2] = digit_of_int(minutes / 10);
  display_string[3] = digit_of_int(minutes % 10);
}

void update_clock(void) {
//  toggle_led1();
//  toggle_led2();
  update_coil_seconds();
  clock_seconds++;
  if (clock_seconds > 59) {
    clock_minutes++;
    clock_seconds = 0;
    if (clock_minutes > 59) {
      clock_hours++;
      clock_minutes = 0;
      if (clock_hours > 23) {
        clock_hours = 0;
      }
    };
    update_display_string(clock_hours, clock_minutes);
    alarm_check(clock_hours, clock_minutes);
  }

}

void clock_display(uint8_t *shift_data) {
  for (uint8_t cip = 0; cip < NUM_DISPLAY_CHARS + 1; cip++) {
    uint8_t ci = (cip + digit_offset) % NUM_DISPLAY_CHARS;
    uint16_t bits = get_char_bits(display_string[ci]) & ~(1 << get_shift_digit(ci));
    struct byte_pair bytes = bytes_of_uint16_t (bits);
    shift_data[CLOCK_BYTE_H] = bytes.bh;
    shift_data[CLOCK_BYTE_L] = bytes.bl;
    shift_bytes(shift_data);
//    shift_output(*shift_data);
  };
  digit_offset = (digit_offset + NUM_DISPLAY_CHARS + 1) % NUM_DISPLAY_CHARS;
}

void clock_display_flash_hours(uint8_t flash_state, uint8_t *shift_data) {
  uint8_t d0 = display_string[0];
  uint8_t d1 = display_string[1];
  if (!(flash_state)) {
    display_string[0] = '?';
    display_string[1] = '?';
  }

  clock_display(shift_data);

  if (!(flash_state)) {
    display_string[0] = d0;
    display_string[1] = d1;
  }
}

void clock_display_flash_minutes(uint8_t flash_state, uint8_t *shift_data) {
  uint8_t d0 = display_string[2];
  uint8_t d1 = display_string[3];
  if (!(flash_state)) {
    display_string[2] = '?';
    display_string[3] = '?';
  }

  clock_display(shift_data);

  if (!(flash_state)) {
    display_string[2] = d0;
    display_string[3] = d1;
  }
}

