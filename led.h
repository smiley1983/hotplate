/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef LEDH
#define LEDH

#include <avr/io.h>

typedef void (*ledset_func) (void);

void init_leds(void);
void led1_on(void);
void led2_on(void);
void led1_off(void);
void led2_off(void);
void led3_on(void);
void led3_off(void);
void led4_on(void);
void led4_off(void);
void led5_on(void);
void led5_off(void);
void led6_on(void);
void led6_off(void);
void toggle_led(ledset_func set_on, ledset_func set_off, uint8_t *state);
void toggle_led1(void);
void toggle_led2(void);
void toggle_led3(void);
void toggle_led4(void);
void toggle_led5(void);
void toggle_led6(void);
void toggle_led7(void);
void toggle_led8(void);
void green1_on(void);
void green1_off(void);
void red1_on(void);
void red1_off(void);
void blue1_on(void);
void blue1_off(void);
void green2_on(void);
void green2_off(void);
void red2_on(void);
void red2_off(void);
void blue2_on(void);
void blue2_off(void);
uint8_t led1_state;
uint8_t led2_state;

#endif
