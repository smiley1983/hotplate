/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef BUTTONH
#define BUTTONH

#include <avr/io.h>
void init_buttons(void);
void read_buttons(void);
void clear_buttons(void);
void clear_button_states(void);

uint8_t b4_pressed(void);
uint8_t b2_pressed(void);
uint8_t b1_pressed(void);
uint8_t b3_pressed(void);

uint8_t up_pressed(void);
uint8_t down_pressed(void);
uint8_t left_pressed(void);
uint8_t right_pressed(void);

#endif
