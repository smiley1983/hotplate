#!/bin/bash
rm submit/*
rm submit.zip
cp hotplate.drl submit/hotplate.TXT
cp hotplate-Edge.Cuts.gbr submit/hotplate.GKO
cp hotplate-B.Cu.gbr submit/hotplate.GBL
cp hotplate-B.Mask.gbr submit/hotplate.GBS
cp hotplate-F.Cu.gbr submit/hotplate.GTL
cp hotplate-F.Mask.gbr submit/hotplate.GTS
cp hotplate-F.SilkS.gbr submit/hotplate.GTO
cp hotplate-B.SilkS.gbr submit/hotplate.GBO
cd submit
zip ../submit.zip *
cd ..

