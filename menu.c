/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */


#include <menu.h>
#include <shift.h>
#include <clock.h>
#include <button.h>
#include <set_time.h>
#include <alarm.h>
#include <led.h>
#include <coil.h>

#define MENU_ITEMS 6 // 7
#define MI_CLOCK 0
#define MI_SET 1
#define MI_AL 2
#define MI_CALI 3
#define MI_3CAL 4
#define MI_SAVE 5
#define MI_MENU 6

uint8_t menu_mode = 0;

uint8_t coil_cycle = 0;

void menu(uint8_t *shift_data) {
  if (menu_mode == MI_MENU) {
    display_string[0] = 'M';
    display_string[1] = 'E';
    display_string[2] = 'N';
    display_string[3] = 'U';
  }
  else if (menu_mode == MI_SET) {
    display_string[0] = 'S';
    display_string[1] = 'E';
    display_string[2] = 'T';
    display_string[3] = '?';
  }
  else if (menu_mode == MI_AL) {
    display_string[0] = 'A';
    display_string[1] = 'L';
    display_string[2] = '?';
    display_string[3] = '?';
  }
  else if (menu_mode == MI_CALI) {
    display_string[0] = 'C';
    display_string[1] = 'A';
    display_string[2] = 'L';
    display_string[3] = 'I';
  }
  else if (menu_mode == MI_3CAL) {
    display_string[0] = '3';
    display_string[1] = 'C';
    display_string[2] = 'A';
    display_string[3] = 'L';
  }
  else if (menu_mode == MI_SAVE) {
    display_string[0] = 'S';
    display_string[1] = 'A';
    display_string[2] = 'V';
    display_string[3] = 'E';
  }
  //clock_display(shift_data); // no shift register used
}

void menu_process_buttons(uint8_t *shift_data) {

  if (b1_pressed()) {
    clear_button_states();
    coilmode_off();
    prev_heat();
  }
  if (b2_pressed()) {
    clear_button_states();
    coilmode_off();
    next_heat();
  }
  if (b3_pressed()) {
    clear_button_states();
    toggle_coilmode();
    if (coil_state()) {
        set_coil_cycle(360);
    }
  }
  if (b4_pressed()) {
    blue1_on();
    red1_off();
    green1_off();
    regulate_coil();
    coilmode_off();
    set_manual_on();
  }
  else {
    //red1_on();
    //blue1_off();
    //coil_off();
    unset_manual_on();
    if (coil_state()) {
      regulate_coil();
    }
    else {
      coil_off();
    };
    update_coil_setting_leds();
  }


/*
  if ((menu_mode == 0) && left_pressed() && (led1_state || led2_state)) {
    clear_button_states();
    led1_off();
    led2_off();
  }
  else if ((menu_mode == 0) && up_pressed()) {
    clear_button_states();
    led1_on();
  }
  else if ((menu_mode == 0) && down_pressed()) {
    clear_button_states();
    led2_on();
  }

  if (menu_mode > 0 && menu_mode < MENU_ITEMS) {
    menu(shift_data);
  };

  if (menu_mode > 0 && menu_mode < MENU_ITEMS && left_pressed()) {
    menu_mode --;
    clear_button_states();
    if (menu_mode == 0) {
      update_display_string(clock_hours, clock_minutes);
    }
  }
  else if (menu_mode < MENU_ITEMS - 1 && right_pressed()) {
    menu_mode ++;
    clear_button_states();
  }
  else if (menu_mode == MI_SET && down_pressed()) {
    clear_button_states();
    menu_mode = MENU_ITEMS + MI_SET;
    begin_setting_time(clock_hours, clock_minutes);
  }
  else if (menu_mode == MENU_ITEMS + MI_SET) {
    set_time_process_buttons(&menu_mode, shift_data, set_clock_time);
  }
  else if (menu_mode == MI_AL && down_pressed()) {
    clear_button_states();
    menu_mode = MENU_ITEMS + MI_AL;
    begin_setting_time(clock_hours, clock_minutes);
  }
  else if (menu_mode == MENU_ITEMS + MI_AL) {
    set_time_process_buttons(&menu_mode, shift_data, set_alarm_time);
  }
  */
}
