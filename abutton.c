/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

/*
 * abutton is for reading many buttons on one analog pin
 */

#include <abutton.h>
#include <clock.h>

struct abutton btn;

uint8_t btn_init = 0;

void init_abutton() {

  // set initial values
  btn.reading = 0;
  btn.pressed = 0;
  btn.value_ready = 0;
  btn.value = 0;
  btn.debounce = 0;
  btn.repeat = 0;

  #ifdef CRYSTAL
    // set prescaler to 128
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0);
  #else
    // set prescaler to 16
    ADCSRA |= (1 << ADPS2);
  #endif

  // use AVCC for reference voltage
  ADMUX |= (1 << REFS0);

  // use ADC0
  ADMUX &= ~((1 << MUX3) | (1 << MUX2) | (1 << MUX1) | (1 << MUX0));

  // free running mode
  #ifdef ATMEGA328P
    ADCSRB &= ~((1 << ADTS0) | (1 << ADTS1) | (1 << ADTS2));
  #else
    ADCSRA |= (1 << ADFR);
  #endif

  // enable ADC
  ADCSRA |= (1 << ADEN);

  // begin sampling
  ADCSRA |= (1 << ADSC);

}

/*
uint8_t adc_to_value(uint16_t v) {
  if (v < MIN_PRESS) {
    return NOTHING;
  }
  else if (v < B01_VAL) {
    return B01;
  }
  else if (v < B02_VAL) {
    return B02;
  }
  else if (v < B03_VAL) {
    return B03;
  }
  else if (v < B04_VAL) {
    return B04;
  }
  else if (v < B05_VAL) {
    return B05;
  }
  else if (v < B06_VAL) {
    return B06;
  }
  else if (v < B07_VAL) {
    return B07;
  }
  else if (v < B08_VAL) {
    return B08;
  }
  else if (v < B09_VAL) {
    return B09;
  }
  else if (v < B10_VAL) {
    return B10;
  }
  else if (v < B11_VAL) {
    return B11;
  }
  else { //if (v < B12_VAL) {
    return B12;
  }
}
*/

void update_abutton_reading(void) {
  //cli(); // disable interrupts
  btn.reading = ADCH;  //
  btn.reading <<= 8;   //
  btn.reading |= ADCL; // update ADC reading
  //sei(); // enable interrupts
  display_four_digit_int(btn.reading);
}

uint16_t get_abutton_reading(void) {
  return btn.reading;
}

/*
void update_button(struct abutton b) {
  update_reading(b);
  b.pressed = b.reading >= MIN_PRESS;
  if (b.debounce == 0) {
    b.value_ready = 0;
  };
  if ((b.debounce == 0) | (b.debounce == 1)) {
    if (b.pressed) {
      b.debounce = DEBOUNCE_TICKS;
    }
  }
  else {
    b.debounce--;
    if (b.pressed && !(b.value_ready) && (b.debounce < DEBOUNCE_SETTLE)) {
      b.value = adc_to_value(b.reading);
      b.value_ready = 1;
    }
  }
}

uint8_t read_abutton(struct abutton b) {
  update_button(b);
  if (b.value_ready) {
    return b.value;
  }
  else {
    return NOTHING;
  }
}

uint8_t read_test_btn(void) {
  if (btn_init == 0) {
    init_abutton(btn);
    btn_init = 1;
  };
  return read_abutton(btn);
}

int abutton_pressed(struct abutton b) {
  return 0;
}
*/
