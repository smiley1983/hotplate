/*
 *  Copyright 2016 Jude Hungerford

 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at

 *      http://www.apache.org/licenses/LICENSE-2.0

 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#ifndef SETTIMEH
#define SETTIMEH

#include <avr/io.h>

typedef void (*timeset_func) (uint8_t, uint8_t);

void set_time_process_buttons(uint8_t *menu_mode, uint8_t *shift_data, timeset_func set_time);
void begin_setting_time(uint8_t h, uint8_t m);

#endif
